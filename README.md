oracle-rdbms-server-11gR2-preinstall
====================================

This is a modified version of oracle-rdbms-server-11gR2-preinstall that is 
adapted to use accounts not in /etc/passwd.  It also does not require the use 
of Oracle's kernel. The original version can be found at 
<http://public-yum.oracle.com/repo/OracleLinux/OL6/latest/x86_64/>

Please note that the source for this package has moved from GitHub to Bitbucket
as a result of GitHub doing away with the hosting of files at /downloads.  You
can find the most recent version at
<https://bitbucket.org/genebean/oracle-rdbms-server-11gr2-preinstall>
